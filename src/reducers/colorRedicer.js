import { ADD_COLOR, RATING_COLOR, REMOVE_COLOR } from '../actions/constants'

export const colorReducer = (state = {}, action) => {
  switch (action.type) {
    case ADD_COLOR:
      return {
        id: action.id,
        title: action.title,
        color: action.color,
        timestamp: action.timestamp,
        rating: 0
      }
    case RATING_COLOR:
      return (state.id !== action.id) ?
        state :
        {
          ...state,
          rating: action.rating
        }
    default:
      return state
  }
}

export const colorsReducer = ( state = [], action) => {
  switch (action.type) {
    case ADD_COLOR:
      return [
        ...state,
        colorReducer({}, action)
      ]
    case RATING_COLOR:
      return state.map(
        c => colorReducer(c, action)
      )
    case REMOVE_COLOR:
      return state.filter(c => c.id !== action.id)
    default:
      return state
  }
}