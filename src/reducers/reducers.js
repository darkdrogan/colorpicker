import { colorReducer } from './colorRedicer'
import { combineReducers } from 'redux'

export default combineReducers({
  colorReducer
})