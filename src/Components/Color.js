import React from 'react'
import PropTypes from 'prop-types'
import StarRating from './StarRating';
import './Color.css'

// const Color = ({title, color, rating=0, onRemove=f=>f, onRate=f=>f}) =>
//   <section className='color'>
//     <h1>{title}</h1>
//     <button onClick={onRemove}>X</button>
//     <div className='color' style={{backgroundColor: color}}> <br/>
//     </div>
//     <div>
//       <StarRating starsSelected={rating} onRate={onRate}/>
//     </div>
//   </section>

class Color extends React.Component {
  componentWillMount() {
    this.style = { backgroundColor: '#CCC' }
  }

  componentWillUpdate() {
    this.style = null
  }

  shouldComponentUpdate(nextProps) {
    const { rating } = this.props
    return rating !== nextProps.rating
  }

  render() {
    const { title, rating, color, onRate, onRemove } = this.props
    return (
      <section className='color' style={this.style}>
        <h1 ref='title'>{title}</h1>
        <button onClick={onRemove}>X</button>
        <div className='color' style={{ backgroundColor: color }}></div>
        <StarRating starsSelected={rating} onRate={onRate}/>
      </section>
    )
  }
}

Color.propTypes = {
  title: PropTypes.string,
  rating: PropTypes.number,
  color: PropTypes.string,
  onRate: PropTypes.func,
  onRemove: PropTypes.func
}

Color.defaultProps = {
  title: undefined,
  rating: 0,
  color: '#000000',
  onRate: f => f,
  onRemove: f => f
}

export default Color;