import { ADD_COLOR, RATING_COLOR, REMOVE_COLOR } from './constants'
import { v4 } from 'uuid'

export const addColor = (title, color) => ({
  type: ADD_COLOR,
  id: v4(),
  title,
  color,
  timestamp: new Date().toString()
})

export const removeColor = (id) => ({
  type: REMOVE_COLOR,
  id
})

export const rateColor = (id, rate) => ({
  type: RATING_COLOR,
  id,
  rate
})
