import React from 'react';
import ReactDOM from 'react-dom';
// import AddColorFormF from './Components/AddColorFormF';

// import './index.css';
import App from './App';
// import PeopleList from './HOC/PeopleList'
import registerServiceWorker from './registerServiceWorker';
// import StarRating from './Components/StarRating';

ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<PeopleList/>, document.getElementById('root'));
// ReactDOM.render(<StarRating />, document.getElementById('root'));
registerServiceWorker();
